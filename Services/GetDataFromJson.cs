﻿using Avalonia.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection.Metadata;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace bannerApp.Models
{
    public class GetDataFromJson
    {
        /// <summary>
        ///  GetSchedule: Is the class that will be used for extracting schedule from .json file
        ///  into a string
        /// </summary>  
        public List<string> GetSchedule(string filename, List<String> stopsList)
        {
            if (stopsList is null)
            {
                throw new ArgumentNullException(nameof(stopsList));
            }

            var jsonString = File.ReadAllText(filename);

            try{
                using JsonDocument doc = JsonDocument.Parse(jsonString);
                JsonElement root = doc.RootElement;
                JsonElement schedule = root.GetProperty("Stops");

                foreach (JsonElement stop in schedule.EnumerateArray())
                {
                    if (stop.TryGetProperty("Name", out JsonElement stopName))
                    {
                        stopsList.Add(stopName.ToString());
                    }
                }

            } catch (FileNotFoundException e)
            {
                throw e;
            }

            //string cleaned = allStops.ToString().Replace("\n", "").Replace("\r", "");
            return stopsList;
        } 

        /// <summary>
        ///  GetBusLine: Is the class that will be used for extracting bus line number
        ///  from .json onto a string
        /// </summary>  
        public string GetBusLine(string filename)
        {
            var jsonString = File.ReadAllText(filename);
            var busLine = new System.Text.StringBuilder();
            busLine.Append("S1");

            try{
                using JsonDocument doc = JsonDocument.Parse(jsonString);
                JsonElement root = doc.RootElement;
                JsonElement schedule = root.GetProperty("Stops");

                foreach (JsonElement stop in schedule.EnumerateArray())
                {
                    if (stop.TryGetProperty("LineNo", out JsonElement lineNo))
                    {
                        busLine.Append(lineNo.GetString());
                    }

                }
            } catch (FileNotFoundException e)
            {
                throw e;
            }

            return busLine.ToString();
        }
    }
}
