using Avalonia;
using Avalonia.Animation;
using Avalonia.Controls;
using Avalonia.Data;
using Avalonia.Markup.Xaml;
using Avalonia.Media;
using bannerApp.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;


namespace bannerApp.Views
{
    public class MainWindow : Window
    {
        /// <summary>
        ///                             dockPanel
        ///  -------------------------------------------------------------------
        ///  |     leftDock                  |              rightDock           |
        ///  |------------------------------------------------------------------| <- height
        ///  |                                                                  |
        ///  --------------------------------------------------------------------
        ///                          width
        /// </summary
        

        private DockPanel dockPanel;
        private DockPanel leftDock;
        //private DockPanel rightDock;
       
        public double height;
        public double width;
        //private double topPanelHeight;

        public Database db;
        public DataHandler dataSet;
        private readonly GetDataFromJson JsonHandler = new GetDataFromJson();

        private List<string> schedule;
        private List<TextBlock> scheduleElements;
        private short currentStop = 4; //fix later to get data from .json file

        public MainWindow()
        { 
        
            db = new Database
            {
                BusLine = "-1"
            };

            schedule = new List<string>();
            scheduleElements = new List<TextBlock>();
            InitializeComponent();
            SetUpWindowProperties();
            SetUpLayout();
        }
        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        /// <summary>
        ///  SetUpLayout: Setups diffrent parts of layout
        /// </summary>
        private void SetUpLayout()
        {
            SetUpDockPanel();
            SetUpLeftDock();
            SetUpRightDock();
        }

        public void SetUpWindowProperties() // Set ups window properties, like it size
        {
            height = 768;
            width = 1366;

            //topPanelHeight = 220;

            this.WindowState = WindowState.Maximized;
            this.HasSystemDecorations = false;
            this.Topmost = false;
        }

        private void SetUpDockPanel()
        {
            dockPanel = this.FindControl<DockPanel>("mainPanel"); 
            //dockPanel.Height = height;
        }

        private void SetUpRightDock()
        {
            var scheduleText = this.FindControl<TextBlock>("Schedule");
            var canvas1 = this.FindControl<Canvas>("canvas1");

            //TextBlock test = new TextBlock
            //{
            //        Background = Brushes.AliceBlue,
            //        Text = "test"
            //};

            //canvas1.Children.Add(test);

            //Canvas.SetLeft(test, 900); // will be usefull for animations

            schedule = JsonHandler.GetSchedule("data.json", schedule);

            System.Text.StringBuilder stops = new System.Text.StringBuilder(); // for testing purposes
            for (int i = 0; i < schedule.Count; i++)
            {
                TextBlock holder = new TextBlock
                {
                    Background = Brushes.AliceBlue,
                    Text = schedule[i],
                    FontSize = 20,
                    
                    //Classes = "animated",
                };
                
                scheduleElements.Add(holder);
                canvas1.Children.Add(holder);

                if(i == currentStop)
                {
                    holder.Foreground = Brushes.Red;
                }
                

                Canvas.SetLeft(holder, 900 - i*100);
                Canvas.SetBottom(holder, 50);
                //string itr = schedule[i];
                //stops.Append(itr);
            }

             Animation a = new Animation();
             
            
            scheduleText.Text = stops.ToString();// for testing purposes
        }

        private void SetUpLeftDock()
        {
            leftDock = this.FindControl<DockPanel>("leftDock");
            leftDock.Background = Brushes.White;

            dataSet = new DataHandler
            {
                BusLine = JsonHandler.GetBusLine("data.json"),
            };

            var busLine = this.FindControl<TextBlock>("busLine");
            busLine.FontSize = 128;
            busLine.Text = dataSet.BusLine;

            var direction = this.FindControl<DockPanel>("direction");
            direction.Background = Brushes.White;
        }
    }
}